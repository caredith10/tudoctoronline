import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate, useParams } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const CitasEditar = () => {

    const navigate = useNavigate();

    const { idcita } = useParams();
    let arreglo = idcita.split('@');
    
    const fechaCita = arreglo[1];
    const horaCita = arreglo[2];
    const lugarCita = arreglo[3];
    const idmedicoCita = arreglo[4];
    const idpacienteCita = arreglo[5];




    console.log(arreglo);

    const [cita, setCita] = useState({
        fecha: fechaCita,
        hora: horaCita,
        lugar:lugarCita,
        idmedico: idmedicoCita,
        idpaciente: idpacienteCita

    });

    const { fecha, hora, lugar, idmedico, idpaciente } = cita;

    useEffect(() => {
        document.getElementById("fecha").focus();
    }, [])

    const onChange = (e) => {
        setCita({
            ...cita,
            [e.target.name]: e.target.value
        })
    }

    const editarCita = async () => {
        let arreglo = idcita.split('@');
        const idCita = arreglo[0];
        

        const data = {
            fecha: cita.fecha,
            hora: cita.hora,
            lugar: cita.lugar,
            idmedico: cita.idmedico,
            idpaciente: cita.idpaciente

        }

        const response = await APIInvoke.invokePUT(`/api/citas/${idCita}`, data);
        const idCitaEditada = response._id

        if (idCitaEditada !== idCita) {
            const msg = "La cita no fue editada correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });

        } else {
            navigate("/citas-admin");
            const msg = "La cita fue editada correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        editarCita();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Edición de Citas"}
                    breadCrumb1={"Listado de Citas"}
                    breadCrumb2={"Edición"}
                    ruta1={"/citas-admin"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                            <form onSubmit={onSubmit}>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Fecha</label>
                                        <input type="date"
                                            className="form-control"
                                            id="fecha"
                                            name="fecha"
                                            placeholder="Ingrese la fecha"
                                            value={fecha}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Hora</label>
                                        <input type="time"
                                            className="form-control"
                                            id="hora"
                                            name="hora"
                                            placeholder="Ingrese la hora"
                                            value={hora}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Lugar</label>
                                        <input type="text"
                                            className="form-control"
                                            id="lugar"
                                            name="lugar"
                                            placeholder="Ingrese el lugar"
                                            value={lugar}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">IdMedico</label>
                                        <input type="number"
                                            className="form-control"
                                            id="idmedico"
                                            name="idmedico"
                                            placeholder="Ingrese el id del medico"
                                            value={idmedico}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">IdPaciente</label>
                                        <input type="number"
                                            className="form-control"
                                            id="idpaciente"
                                            name="idpaciente"
                                            placeholder="Ingrese el id del paciente"
                                            value={idpaciente}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Editar</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default CitasEditar;