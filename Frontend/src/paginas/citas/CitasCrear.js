import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const CitasCrear = () => {

    const navigate = useNavigate();

    const [cita, setCita] = useState({
        fecha: '',
        hora: '',
        lugar:'',
        idmedico: '',
       idpaciente: ''
    });

    const { fecha, hora, lugar, idmedico, idpaciente  } = cita;

    useEffect(() => {
        document.getElementById("fecha").focus();
    }, [])

    const onChange = (e) => {
        setCita({
            ...cita,
            [e.target.name]: e.target.value
        })
    }

    const crearCita = async () => {
        const data = {
            fecha: cita.fecha,
            hora: cita.hora,
            lugar: cita.lugar,
            idmedico: cita.idmedico,
            idpaciente: cita.idpaciente

        }

        const response = await APIInvoke.invokePOST(`/api/citas`, data);
        const idCita = response._id;

        if (idCita === '') {
            const msg = "La cita NO fue creada correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        } else {
            navigate("/citas-admin");
            const msg = "La cita fue creada correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });

            setCita({
                fecha: '',
                hora: '',
                lugar:'',
                idmedico: '',
                idpaciente: ''
             })
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        crearCita();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Creación de Citas"}
                    breadCrumb1={"Listado de Citas"}
                    breadCrumb2={"Creación"}
                    ruta1={"/citas-admin"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                            <form onSubmit={onSubmit}>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="fecha">Fecha</label>
                                        <input type="date"
                                            className="form-control"
                                            id="fecha"
                                            name="fecha"
                                            placeholder="Ingrese la fecha"
                                            value={fecha}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="hora">Hora</label>
                                        <input type="time"
                                            className="form-control"
                                            id="hora"
                                            name="hora"
                                            placeholder="Ingrese la hora"
                                            value={hora}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="lugar">Lugar</label>
                                        <input type="text"
                                            className="form-control"
                                            id="lugar"
                                            name="lugar"
                                            placeholder="Ingrese el lugar"
                                            value={lugar}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="idmedico">IdMedico</label>
                                        <input type="number"
                                            className="form-control"
                                            id="idmedico"
                                            name="idmedico"
                                            placeholder="Ingrese el id del medico"
                                            value={idmedico}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="idpaciente">IdPaciente</label>
                                        <input type="number"
                                            className="form-control"
                                            id="idpaciente"
                                            name="idpaciente"
                                            placeholder="Ingrese el id del paciente"
                                            value={idpaciente}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Crear</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default CitasCrear;