import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate, useParams } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const MedicosEditar = () => {

    const navigate = useNavigate();

    const { idmedico } = useParams();
    let arreglo = idmedico.split('@'); //metodo split
    
    const nombreMedico= arreglo[1];
    const apellidosMedico = arreglo[2];
    const cedulaMedico = arreglo[3];
    const correoMedico = arreglo[4]+"@"+arreglo[5];
    const especialidadMedico = arreglo[6];
    const celularMedico = arreglo[7];




    console.log(arreglo);

    const [medico, setMedico] = useState({
        nombre: nombreMedico,
        apellidos: apellidosMedico,
        cedula:cedulaMedico,
        correo: correoMedico,
        especialidad: especialidadMedico,
        celular: celularMedico
    });

    const { nombre, apellidos, cedula, correo, especialidad, celular} = medico;

    useEffect(() => {
        document.getElementById("nombre").focus();
    }, [])

    const onChange = (e) => {
        setMedico({
            ...medico,
            [e.target.name]: e.target.value
        })
    }

    const editarMedico = async () => {
        let arreglo = idmedico.split('@');
        const idMedico = arreglo[0];
        

        const data = {
            nombre: medico.nombre,
            apellidos: medico.apellidos,
            cedula: medico.cedula,
            correo: medico.correo,
            especialidad: medico.especialidad,
            celular: medico.celular

        }

        const response = await APIInvoke.invokePUT(`/api/medicos/${idMedico}`, data);
        const idMedicoEditado = response._id

        if (idMedicoEditado !== idMedico) {
            const msg = "El registro no fue editado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });

        } else {
            navigate("/medicos-admin");
            const msg = "El registro fue editado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        editarMedico();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Edición de Medicos"}
                    breadCrumb1={"Listado de Medicos"}
                    breadCrumb2={"Edición"}
                    ruta1={"/medicos-admin"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                            <form onSubmit={onSubmit}>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Nombre</label>
                                        <input type="text"
                                            className="form-control"
                                            id="nombre"
                                            name="nombre"
                                            placeholder="Ingrese el nombre"
                                            value={nombre}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Apellidos</label>
                                        <input type="text"
                                            className="form-control"
                                            id="apellidos"
                                            name="apellidos"
                                            placeholder="Ingrese los apellidos"
                                            value={apellidos}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Cedula</label>
                                        <input type="number"
                                            className="form-control"
                                            id="cedula"
                                            name="cedula"
                                            placeholder="Ingrese el número de cédula"
                                            value={cedula}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Correo</label>
                                        <input type="text"
                                            className="form-control"
                                            id="correo"
                                            name="correo"
                                            placeholder="Ingrese el correo"
                                            value={correo}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Especialidad</label>
                                        <input type="text"
                                            className="form-control"
                                            id="especialidad"
                                            name="especialidad"
                                            placeholder="Ingrese la especialidad"
                                            value={especialidad}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Celular</label>
                                        <input type="number"
                                            className="form-control"
                                            id="celular"
                                            name="celular"
                                            placeholder="Ingrese el numero de celular"
                                            value={celular}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Editar</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default MedicosEditar;