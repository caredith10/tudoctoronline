import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate, useParams } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const PacientesEditar = () => {

    const navigate = useNavigate();

    const { idpaciente } = useParams();
    let arreglo = idpaciente.split('@');
    
    const nombrePaciente = arreglo[1];
    const apellidosPaciente = arreglo[2];
    const cedulaPaciente = arreglo[3];
    const generoPaciente = arreglo[4];
    const celularPaciente = arreglo[5];




    console.log(arreglo);

    const [paciente, setPaciente] = useState({
        nombre: nombrePaciente,
        apellidos: apellidosPaciente,
        cedula:cedulaPaciente,
        genero: generoPaciente,
        celular: celularPaciente

    });

    const { nombre, apellidos, cedula, genero, celular } = paciente;

    useEffect(() => {
        document.getElementById("nombre").focus();
    }, [])

    const onChange = (e) => {
        setPaciente({
            ...paciente,
            [e.target.name]: e.target.value
        })
    }

    const editarPaciente = async () => {
        let arreglo = idpaciente.split('@');
        const idPaciente = arreglo[0];
        

        const data = {
            nombre: paciente.nombre,
            apellidos: paciente.apellidos,
            cedula: paciente.cedula,
            genero: paciente.genero,
            celular: paciente.celular

        }

        const response = await APIInvoke.invokePUT(`/api/pacientes/${idPaciente}`, data);
        const idPacienteEditado = response._id

        if (idPacienteEditado !== idPaciente) {
            const msg = "El registro no fue editado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });

        } else {
            navigate("/pacientes-admin");
            const msg = "El registro fue editado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        editarPaciente();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Edición de Pacientes"}
                    breadCrumb1={"Listado de Pacientes"}
                    breadCrumb2={"Edición"}
                    ruta1={"/pacientes-admin"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                            <form onSubmit={onSubmit}>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Nombre</label>
                                        <input type="text"
                                            className="form-control"
                                            id="nombre"
                                            name="nombre"
                                            placeholder="Ingrese el nombre"
                                            value={nombre}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Apellidos</label>
                                        <input type="text"
                                            className="form-control"
                                            id="apellidos"
                                            name="apellidos"
                                            placeholder="Ingrese los apellidos"
                                            value={apellidos}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Cedula</label>
                                        <input type="number"
                                            className="form-control"
                                            id="cedula"
                                            name="cedula"
                                            placeholder="Ingrese el número de cedula"
                                            value={cedula}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Genero</label>
                                        <input type="text"
                                            className="form-control"
                                            id="genero"
                                            name="genero"
                                            placeholder="Ingrese el genero"
                                            value={genero}
                                            onChange={onChange}
                                            required
                                        />
                                        
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Celular</label>
                                        <input type="number"
                                            className="form-control"
                                            id="celular"
                                            name="celular"
                                            placeholder="Ingrese el número del celular"
                                            value={celular}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Editar</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default PacientesEditar;