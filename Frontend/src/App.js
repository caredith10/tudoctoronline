import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import React, {Fragment} from 'react'; 
import Login from './paginas/auth/Login';  
import CrearCuenta from './paginas/auth/CrearCuenta';
import Home from './paginas/Home';
import CitasCrear from "./paginas/citas/CitasCrear";
import CitasAdmin from "./paginas/citas/CitasAdmin";
import CitasEditar from "./paginas/citas/CitasEditar";
import MedicosAdmin from "./paginas/medicos/MedicosAdmin";
import MedicosCrear from "./paginas/medicos/MedicosCrear";
import MedicosEditar from "./paginas/medicos/MedicosEditar";
import PacientesAdmin from "./paginas/pacientes/PacientesAdmin";
import PacientesCrear from "./paginas/pacientes/PacientesCrear";
import PacientesEditar from "./paginas/pacientes/PacientesEditar";


 function App() {   
   return (     
    <Fragment>       
      <Router>          
         <Routes>              
            <Route path="/" exact element={<Login/>}/>
            <Route path="/crear-cuenta" exact element={<CrearCuenta/>}/> 
            <Route path="/home" exact element={<Home/>}/>
            <Route path="/citas-admin" exact element={<CitasAdmin/>}/>
            <Route path="/citas-crear" exact element={<CitasCrear/>}/>
            <Route path="/citas-editar/:idcita" exact element={<CitasEditar/>}/>
            <Route path="/medicos-admin" exact element={<MedicosAdmin/>}/>
            <Route path="/medicos-crear" exact element={<MedicosCrear/>}/>
            <Route path="/medicos-editar/:idmedico" exact element={<MedicosEditar/>}/>
            <Route path="/pacientes-admin" exact element={<PacientesAdmin/>}/>
            <Route path="/pacientes-crear" exact element={<PacientesCrear/>}/>
            <Route path="/pacientes-editar/:idpaciente" exact element={<PacientesEditar/>}/>



         </Routes>
      </Router>
    </Fragment>
  );
}

export default App;

