import React from 'react';
 import ReactDOM from 'react-dom/client'; 
 import './index.css';
 import App from './App';
 import reportWebVitals from './reportWebVitals';

//function formatName(user) {   return user.firstName + ' ' + user.lastName; } 
//const user = {   firstName: 'Carmen',   lastName: 'Edith Arias' }; 
//const element = (<h1>Hello, {formatName(user)}!

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

reportWebVitals();


