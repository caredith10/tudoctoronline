const express = require('express');
const conectarDB = require('../config/db');
const cors = require ('cors');


const app = express();
const port = 7000;

// conectar DB
conectarDB();
app.use(cors());
app.use(express.json());

//Rutas o edpoints (arranque))

app.use('/api/citas', require('../routes/citas'));
app.use('/api/medicos', require('../routes/medicos')); 
app.use('/api/pacientes', require('../routes/pacientes'));
app.use('/api/administradores', require('../routes/administradores'));
app.use('/api/auth', require('../routes/auth'));





//muestra mensaje en el navegador 
//Rutas
app.get("/", (req,res) => {
    res.send("Bienvenidos esta configurado su servidor");
});

//Añadir para mi servidor escuche por un puerto
app.listen(port,() => console.log('El servidor esta conectado',port));