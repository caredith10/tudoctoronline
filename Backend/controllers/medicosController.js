const Medicos = require("../models/Medicos");


exports.crearMedico = async (req,res) => {

    try{
        let medicos;
         // creamos 
         medicos = new Medicos(req.body);
         await medicos.save();
         res.send(medicos);

}catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.mostrarMedicos =async (req,res) =>{

try{
   const medicos = await Medicos.find();
  res.json(medicos)

} catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.obtenerMedico = async (req,res) =>{
 try{
let medicos = await medicos.findById(req.params.id);
if (!medicos){
    res.status(404).json({msg: 'el medico no está en la base de datos'})
}
res.json(medicos);

 }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.eliminarMedico = async (req,res) =>{
    try{
        let medicos = await Medicos.findById(req.params.id);
        if (!medicos){
            res.status(404).json({msg: 'Registro eliminado con exito'})
        }
        await Medicos.findByIdAndRemove({ _id: req.params.id})
        res.json({msg: 'Registro eliminado con exito'});
}catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.actualizarMedico = async (req,res) =>{
    try{
     const {nombre, apellidos, cedula, correo, especialidad, celular} = req.body;
     let medicos = await Medicos.findById(req.params.id); 
     if (!medicos){
        res.status(404).json({msg: 'el medico no está registrado en la base de datos'})
    }
    medicos.nombre = nombre;
    medicos.apellidos= apellidos;
    medicos.cedula = cedula;
    medicos.correo = correo;
    medicos.especialidad = especialidad;
    medicos.celular = celular;

    medicos = await Medicos.findOneAndUpdate({_id: req.params.id}, medicos, {new:true})
    res.json(medicos);

    }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}