const Pacientes = require("../models/Pacientes");


//Métodos

exports.crearPaciente = async (req,res) => {

    try{
        let pacientes;
         // creamos 
         pacientes = new Pacientes(req.body);
         await pacientes.save();
         res.send(pacientes);

}catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.mostrarPacientes =async (req,res) =>{

try{
   const pacientes = await Pacientes.find();
  res.json(pacientes)

} catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}


exports.obtenerPaciente = async (req,res) =>{
    try{
   let pacientes = await Pacientes.findById(req.params.id);
   if (!pacientes){
       res.status(404).json({msg: 'el paciente no está registrado en la base de datos'})
   }
   res.json(pacientes);
   
    }catch (error){
       console.log(error)
       res.status(500).send("hay un error al recibir los datos");
   
   }
   }
   
   exports.eliminarPaciente = async (req,res) =>{
       try{
           let pacientes = await Pacientes.findById(req.params.id);
           if (!pacientes){
               res.status(404).json({msg: 'el medico no existe'})
           }
           await Pacientes.findByIdAndRemove({ _id: req.params.id})
           res.json({msg: 'Registro eliminado con exito'});
   }catch (error){
       console.log(error)
       res.status(500).send("hay un error al recibir los datos");
   
   }
   }
   
   exports.actualizarPaciente = async (req,res) =>{
       try{
        const {nombre, apellidos, cedula, genero, celular} = req.body;
        let pacientes = await Pacientes.findById(req.params.id); 
        if (!pacientes){
           res.status(404).json({msg: 'el paciente no está registrado en la base de datos'})
       }
       pacientes.nombre = nombre;
       pacientes.apellidos= apellidos;
       pacientes.cedula = cedula;
       pacientes.genero = genero;
       pacientes.celular = celular;
   
       pacientes = await Pacientes.findOneAndUpdate({_id: req.params.id}, pacientes, {new:true})
       res.json(pacientes);
   
       }catch (error){
       console.log(error)
       res.status(500).send("hay un error al recibir los datos");
   }
   }