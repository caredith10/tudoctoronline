const Administradores = require("../models/administradores");
const bcryptjs = require("bcryptjs");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");

exports.autenticarAdministrador = async (req, res) => {
  //Revisar si hay errores

  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }

  const { email, password } = req.body;

  try {
    //revisar que sea un usuario registrado
    let administradores = await Administradores.findOne({email });
    if (!administradores) {
      return res.status(400).json({ msg: "El Administrador no existe" });
    }

    //revisar la password
    const passCorrecto = await (password, administradores.password);
    if (!passCorrecto) {
      return res.status(400).json({ msg: "Contraseña incorrecta" });
    }

    //Si todo es correcto, crear y firmar el token

    const payload = {
      administradores: { id: administradores.id },
    };

    jwt.sign(
      payload,
      process.env.SECRETA,
      {
        expiresIn: 43200, //12  horas  43.200 segundos
      },
      (error, token) => {
        if (error) throw error;

        //Mensaje de confirmación
        res.json({ token });
      }
    );
  } catch (error) {
    console.log("Hubo un error");
    console.log(error);
    res.status(400).send("Hubo un error");
  }
};

exports.administradorAutenticado = async (req, res) => {
  try {
    const administradores = await Usuario.findById(req.administradores.id);
    res.json({ administradores });
  } catch (error) {
    res.status(500).json({ msg: "Hubo un error" });
  }
};
