const Citas = require("../models/Citas");

exports.crearCita = async (req,res) => {

    try{
        let citas;
         // creamos las citas
         citas = new Citas(req.body);
         await citas.save();
         res.send(citas);

}catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.mostrarCitas =async (req,res) =>{

try{
   const citas = await Citas.find();
  res.json(citas)

} catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.obtenerCita = async (req,res) =>{
 try{
let citas = await Citas.findById(req.params.id);
if (!citas){
    res.status(404).json({msg: 'No hay cita'})
}
res.json(citas);

 }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.eliminarCita = async (req,res) =>{
    try{
        let citas = await Citas.findById(req.params.id);
        if (!citas){
            res.status(404).json({msg: ' no existe'})
        }
        await Citas.findByIdAndRemove({ _id: req.params.id})
        res.json({msg: 'cita eliminada con exito'});
}catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.actualizarCita = async (req,res) =>{
    try{
     const {fecha, hora, lugar, idmedico, idpaciente} = req.body;
     let citas = await Citas.findById(req.params.id); 
     if (!citas){
        res.status(404).json({msg: ' El código no existe'})
    }
    citas.fecha = fecha;
    citas.hora = hora;
    citas.lugar = lugar;
    citas.idmedico = idmedico;
    citas.idpaciente = idpaciente;

    citas = await Citas.findOneAndUpdate({_id: req.params.id}, citas, {new:true})
    res.json(citas);

    }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}
