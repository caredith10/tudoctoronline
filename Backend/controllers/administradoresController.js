const Administradores = require("../models/administradores");//modelo llamado administradores
const bcryptjs = require("bcryptjs");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");


exports.crearAdministradores = async (req, res) => {

    //revisar si hay errores
    const errores = validationResult(req);
    if (!errores.isEmpty()) {
        return res.status(400).json({ errores: errores.array() });
    }

    const { email, password } = req.body;
    try {

        //Verificamos si el usuario ya existe
        let administradores = await Administradores.findOne({ email });
        if (administradores) {
            return res.status(400).json({ msg: "El usuario ya existe" });
        }
        // creamos nuestro Usuario
        administradores = new Administradores(req.body);
        administradores.password = await bcryptjs.hash(password, 10);
        await administradores.save();
        res.send(administradores);

        //Firmar el JWT
        const payload = {
            administradores: { id: administradores.id },
        };

        jwt.sign(
            payload,
            process.env.SECRETA,
            {
                expiresIn: 3600, //1 hora
            },
            (error, token) => {
                if (error) throw error;

                //Mensaje de confirmación
                res.json({ token });
            }
        );

    } catch (error) {
        console.log(error);
        res.status(500).send("hay un error al recibir los datos");
    }
}


exports.mostrarAdministradores =async (req,res) =>{

try{
   const administradores = await Administradores.find(); //
  res.json(administradores)

} catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.obtenerAdministradores = async (req,res) =>{
 try{
let administradores = await Administradores.findById(req.params.id);
if (!administradores){
    res.status(404).json({msg: 'No se encontro el administrador'})
}
res.json(administradores);

 }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.eliminarAdministradores = async (req,res) =>{
    try{
        let administradores = await Administradores.findById(req.params.id);
        if (!administradores){
            res.status(404).json({msg: ' no existe'})
        }
        await Administradores.findByIdAndRemove({ _id: req.params.id})
        res.json({msg: ' eliminado con exito'});
}catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.actualizarAdministradores = async (req,res) =>{
    try{
     const {nombre, email, password} = req.body;
     let administradores = await Administradores.findById(req.params.id); 
     if (!administradores){
        res.status(404).json({msg: 'Ese código no existe'})
    }
    administradores.nombre = nombre;
    administradores.email = email;
    administradores.password = password;
   
   administradores = await Administradores.findOneAndUpdate({_id: req.params.id}, administradores, {new:true})
    res.json(administradores);

    }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}
