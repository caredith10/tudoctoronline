// Armamos las Rutas pacientes

const express = require('express');
const router = express.Router();
const pacientesController = require('../controllers/pacientesController');


// rutas CRUD

router.get('/', pacientesController.mostrarPacientes);
router.post('/', pacientesController.crearPaciente);
router.get('/:id', pacientesController.obtenerPaciente);
router.put('/:id', pacientesController.actualizarPaciente);
router.delete('/:id', pacientesController.eliminarPaciente);

module.exports = router;

