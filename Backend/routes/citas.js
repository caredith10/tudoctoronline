// Armamos las Rutas para crear las citas

const express = require('express');
const router = express.Router();
const citasController = require('../controllers/citasController');

// rutas CRUD

router.get('/', citasController.mostrarCitas);
router.post('/', citasController.crearCita);
router.get('/:id', citasController.obtenerCita);
router.put('/:id', citasController.actualizarCita);
router.delete('/:id', citasController.eliminarCita);

module.exports = router;
