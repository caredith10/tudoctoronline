// Armamos las Rutas de la colección administradores

const express = require('express');
const router = express.Router();
const administradoresController = require('../controllers/administradoresController');


// rutas CRUD

router.get('/', administradoresController.mostrarAdministradores);
router.post('/', administradoresController.crearAdministradores);
router.get('/:id', administradoresController.obtenerAdministradores);
router.put('/:id', administradoresController.actualizarAdministradores);
router.delete('/:id', administradoresController.eliminarAdministradores);

module.exports = router;
