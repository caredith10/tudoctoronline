// Armamos las Rutas medicos

const express = require('express');
const router = express.Router();
const medicosController = require('../controllers/medicosController');


// rutas CRUD

router.get('/', medicosController.mostrarMedicos);
router.post('/', medicosController.crearMedico);
router.get('/:id', medicosController.obtenerMedico);
router.put('/:id', medicosController.actualizarMedico);
router.delete('/:id', medicosController.eliminarMedico);

module.exports = router;

