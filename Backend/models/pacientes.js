const mongoose = require('mongoose');

const pacienteSchema = mongoose.Schema({

    nombre:{
        type: String,
        required: true
    },
    apellidos:{
        type: String,
        required: true
    },
    cedula:{
        type: Number,
        required: true
    },
    genero:{
        type: String,
        required: true
    },
    celular:{
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Pacientes', pacienteSchema );