const mongoose = require('mongoose');

const citasSchema = mongoose.Schema({
    fecha:{
        type: String,
        required: true
    },
    hora:{
        type: String,
        required: true
    },

    
    lugar:{
        type: String,
        required: true
    },
    idmedico:{
        type: Number,
        required: true
    },
    idpaciente:{
        type: Number,
        required: true
    },

    
});

module.exports = mongoose.model('Citas', citasSchema );