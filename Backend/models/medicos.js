const mongoose = require('mongoose');

const medicosSchema = mongoose.Schema({

    nombre:{
        type: String,
        required: true
    },

    apellidos:{
        type: String,
        required: true
    },
    cedula:{
        type: Number,
        required: true
    },
    correo:{
        type: String,
        required: true
    },

    especialidad:{
        type: String,
        required: true
    },
    celular:{
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Medicos', medicosSchema );